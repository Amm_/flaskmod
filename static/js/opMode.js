var opMode = {};

opMode.init = function() {
	if(document.getElementById('threadIdentifier')){
	    let linkOPMode = document.createElement('a');
	    linkOPMode.id = "linkOPMode";
	    linkOPMode.classList.add("icon");
	    linkOPMode.classList.add("peopleIcon");
	    linkOPMode.title = "Show only OP posts/Show all posts";

	    let navImages = document.getElementById("navOptionsSpanThreadImages");
	    navImages.insertBefore(linkOPMode,navImages.firstChild);
	    navImages.classList.remove("hidden");

	    var originalPoster = document.getElementsByClassName("opHead")[0].getElementsByClassName("linkName")[0].innerText;
	    
	    linkOPMode.onclick = function() {
		    linkOPMode.classList.toggle("peopleIcon");
		    linkOPMode.classList.toggle("personIcon");

		    let modeEnabled = linkOPMode.classList.contains("personIcon") ? true : false;

		    Array.from(document.getElementsByClassName("postCell")).forEach(function(post){
		    	if(modeEnabled){
			    	if(originalPoster !== post.getElementsByClassName("linkName")[0].innerText){
			    		post.classList.add("hidden");
			    	}
			    } else {
			    	post.classList.remove("hidden");
			    }
	  		});
    	};
	}

}

opMode.init();