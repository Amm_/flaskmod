var watcher = {};

watcher.init = function() {

  watcher.isInThread = document.getElementById('threadIdentifier') ? true
      : false;
  watcher.watcherAlertCounter = 0;
  watcher.elementRelation = {};

  var postingLink = document.getElementById('navPosting');
  var referenceNode = postingLink.nextSibling;

  var watcherButton = document.createElement('a');
  watcherButton.id = 'watcherButton';
  watcherButton.className = 'icon';
  watcherButton.title = "Thread Watcher"

  watcher.watcherCounter = document.createElement('span');

  watcherButton.appendChild(watcher.watcherCounter);

  postingLink.parentNode.insertBefore(watcherButton, referenceNode);

  watcher.watchedMenu = document.createElement('div');

  var watchedMenuTitle = document.createElement('div');
  watchedMenuTitle.className = 'watchedMenuTitle';

  var watchedMenuLabel = document.createElement('label');
  watchedMenuLabel.innerHTML = 'Watched threads';

  var storedWatchedData = watcher.getStoredWatchedData();

  var showingWatched = storedWatchedData.showingWatched;
  if(showingWatched) {
    watcher.watchedMenu.classList.remove("hidden");
  } else {
    watcher.watchedMenu.classList.add("hidden");
  }

  var closeWatcherMenuButton = document.createElement('span');
  closeWatcherMenuButton.id = 'closeWatcherMenuButton';
  closeWatcherMenuButton.className = 'icon glowOnHover';
  closeWatcherMenuButton.onclick = function() {

    if (!showingWatched) {
      return;
    }

    showingWatched = false;
    watcher.watchedMenu.classList.add("hidden");

    var storedWatchedData = watcher.getStoredWatchedData();
    storedWatchedData.showingWatched = false;
    localStorage.watchedData = JSON.stringify(storedWatchedData);
  };

  watchedMenuTitle.appendChild(watchedMenuLabel);
  watchedMenuTitle.appendChild(closeWatcherMenuButton);
  watcher.watchedMenu.appendChild(watchedMenuTitle);

  watcher.watchedMenu.id = 'watchedMenu';
  watcher.watchedMenu.classList.add('floatingMenu');

  document.body.appendChild(watcher.watchedMenu);

  watcherButton.onclick = function() {
    var storedWatchedData = watcher.getStoredWatchedData();
    if (!showingWatched) {
      showingWatched = true;
      watcher.watchedMenu.classList.remove("hidden");
      storedWatchedData.showingWatched = true;
      draggable.moveToStoredPlace(watcher.watchedMenu);
    } else {
      showingWatched = false;
      watcher.watchedMenu.classList.add("hidden");
      storedWatchedData.showingWatched = false;
    }
    localStorage.watchedData = JSON.stringify(storedWatchedData);
  }

  var ops = document.getElementsByClassName('innerOP');

  for (var i = 0; i < ops.length; i++) {
    watcher.processOP(ops[i]);
  }

  for ( var currentBoard in storedWatchedData) {
    if (storedWatchedData.hasOwnProperty(currentBoard)) {

      var threads = storedWatchedData[currentBoard];

      for ( var thread in threads) {
        if (threads.hasOwnProperty(thread)) {

          if (watcher.isInThread && currentBoard == api.boardUri
              && thread == api.threadId) {
            threads[thread].lastSeenPostCount = document.getElementsByClassName('postCell').length;
            delete threads[thread].newOP;
            delete threads[thread].replyYou;
            localStorage.watchedData = JSON.stringify(storedWatchedData);
          }

          watcher.addWatchedCell(currentBoard, thread, threads[thread]);
        }
      }
    }

  }

  watcher.updateWatcherCounter();

  watcher.scheduleWatchedThreadsCheck();
  var originalReplyCallback = thread.replyCallback;
  thread.replyCallback = function(status, data){
    watcher.runWatchedThreadsCheck();
    originalReplyCallback(status,data);
  }

  draggable.setDraggable(watcher.watchedMenu, watchedMenuLabel);
  watcher.refresh();
};

watcher.updateWatcherCounter = function() {

  var classList = watcherButton.classList;

  if (watcher.watcherAlertCounter) {
    classList.add('mobileAlert');
  } else {
    classList.remove('mobileAlert');
  }

  watcher.watcherCounter.innerHTML = watcher.watcherAlertCounter ? '('
      + watcher.watcherAlertCounter + ')' : '';
};

watcher.getStoredWatchedData = function() {

  var storedWatchedData = localStorage.watchedData;

  if (storedWatchedData) {
    storedWatchedData = JSON.parse(storedWatchedData);
  } else {
    storedWatchedData = {};
  }

  return storedWatchedData;

};

watcher.refresh = function(){
  var storedWatchedData = watcher.getStoredWatchedData();

  Object.keys(storedWatchedData).forEach(function(board){
    if(board === "showingWatched") return
    let boardData = storedWatchedData[board];
    
    Object.keys(boardData).forEach(function(threadId){
      let watchData = boardData[threadId];

      if (!watcher.elementRelation[board]
          || !watcher.elementRelation[board][threadId]) {
        watcher.addWatchedCell(board, threadId, watchData);
      } else if (watchData.lastSeenPostCount >= watchData.currentPostCount) {
        watcher.elementRelation[board][threadId].style.display = 'none';
      } else {
        watcher.watcherAlertCounter++;
        watcher.elementRelation[board][threadId].style.display = 'inline';
        watcher.elementRelation[board][threadId].innerHTML = "(" + (watchData.currentPostCount - watchData.lastSeenPostCount) + ")";
        if(watchData.newOP){
          watcher.elementRelation[board][threadId].innerHTML += "(OP)";
        }
      }
      if(watchData.replyYou){
        watcher.elementRelation[board][threadId].parentNode.parentNode.classList.add("markedPost");
      } else {
        watcher.elementRelation[board][threadId].parentNode.parentNode.classList.remove("markedPost");
      }
      if(watcher.elementRelation[board][threadId].previousSibling){
        if(watchData.archived){
          watcher.elementRelation[board][threadId].previousSibling.classList.add("archiveIndicator");
        } else {
          watcher.elementRelation[board][threadId].previousSibling.classList.remove("archiveIndicator");
        }
        if(watchData.locked){
          watcher.elementRelation[board][threadId].previousSibling.classList.add("lockIndicator");
        } else {
          watcher.elementRelation[board][threadId].previousSibling.classList.remove("lockIndicator");
        }
      }
    });
  });
}

watcher.processThread = function(urls, index, data) {

  data = JSON.parse(data);
  var url = urls[index];

  var posts = data.posts;
  var storedWatchedData = watcher.getStoredWatchedData();
  var watchData = storedWatchedData[url.board][url.thread];
  if (posts && posts.length) {
    if (posts.length > watchData.currentPostCount) {
      watchData.currentPostCount = posts.length;
      delete watchData.newOP;
      delete watchData.replyYou;
      let newPostCount = watchData.currentPostCount - watchData.lastSeenPostCount;
      let newPosts = posts.slice((posts.length-newPostCount),posts.length);
      if(data.name !== "Anonymous"){
        for (let i = 0; i < newPosts.length; i++) {
          if(newPosts[i].name === data.name){
            watchData.newOP = true;
          } 
          
        }
      }
      let yous = localStorage.getItem(data.boardUri + "-yous");
      yous = (yous === null) ? [] : JSON.parse(yous);
      if(yous.length){
        for (let i = 0; i < newPosts.length; i++) {
          for(var x = 0; x < yous.length; x++){
            if(newPosts[i].message.match(">>"+yous[x])) {
              watchData.replyYou = true;
            }
          }
        }
      }
    } else if(posts.length < watchData.currentPostCount) {
      watchData.currentPostCount = posts.length;
      delete watchData.newOP;
      delete watchData.replyYou;
    }
  }

  if(data.archived){
    watchData.archived = true;
  } else {
    delete watchData.archived;
  }
  if(data.locked){
    watchData.locked = true;
  } else {
    delete watchData.locked;
  }

  localStorage.watchedData = JSON.stringify(storedWatchedData);
  watcher.iterateWatchedThreads(urls, ++index);

};

watcher.iterateWatchedThreads = function(urls, index) {

  index = index || 0;

  if (index >= urls.length) {
    watcher.updateWatcherCounter();
    return;
  }

  var url = urls[index];

  api.localRequest('/' + url.board + '/res/' + url.thread + '.json',
      function gotThreadInfo(error, data) {
        if (error) {
          watcher.iterateWatchedThreads(urls, ++index);
        } else {
          watcher.processThread(urls, index, data);
        }

      });

};

watcher.runWatchedThreadsCheck = function() {

  watcher.watcherAlertCounter = 0;

  localStorage.lastWatchCheck = new Date().getTime();

  var urls = [];

  var storedWatchedData = watcher.getStoredWatchedData();

  for ( var board in storedWatchedData) {

    if (storedWatchedData.hasOwnProperty(board)) {

      var threads = storedWatchedData[board];

      for ( var thread in threads) {
        if (threads.hasOwnProperty(thread)) {

          if (watcher.isInThread && board == api.boardUri
              && thread == api.threadId) {
            threads[thread].lastSeenPostCount = document.getElementsByClassName("postCell").length;
            delete threads[thread].replyYou;
            localStorage.watchedData = JSON.stringify(storedWatchedData);
          }

          urls.push({
            board : board,
            thread : thread
          });
        }
      }
    }

  }

  watcher.iterateWatchedThreads(urls);

};

watcher.scheduleWatchedThreadsCheck = function() {

  var lastCheck = localStorage.lastWatchCheck;

  if (!lastCheck) {
    watcher.runWatchedThreadsCheck();
    return;
  }

  lastCheck = new Date(+lastCheck);

  if(new Date().getTime() - lastCheck.getTime() > 10*1000){
    watcher.runWatchedThreadsCheck();
  }

  setTimeout(function() { 
    watcher.refresh();
    watcher.scheduleWatchedThreadsCheck();
  }, 2*1000);
}

watcher.addWatchedCell = function(board, thread, watchData) {

  var cellWrapper = document.createElement('div');

  var cell = document.createElement('div');
  cell.className = 'watchedCell';

  var labelWrapper = document.createElement('label');
  labelWrapper.className = 'watchedCellLabel';

  var label = document.createElement('a');
  if(watchData.replyYou){
    cell.classList.add("markedPost");
  }
  label.innerHTML = watchData.label || (board + '/' + thread);
  label.href = '/' + board + '/res/' + thread + '.html';
  labelWrapper.appendChild(label);

  var notification = document.createElement('span');
  notification.className = 'watchedNotification';

  if (!watcher.elementRelation[board]) {
    watcher.elementRelation[board] = {};
  }

  watcher.elementRelation[board][thread] = notification;

  labelWrapper.appendChild(notification);

  cell.appendChild(labelWrapper);

  var button = document.createElement('span');
  button.className = 'watchedCellCloseButton glowOnHover icon';
  cell.appendChild(button);

  button.onclick = function() {

    watcher.watchedMenu.removeChild(cellWrapper);

    var storedWatchedData = watcher.getStoredWatchedData();

    var boardThreads = storedWatchedData[board];

    if (!boardThreads || !boardThreads[thread]) {
      return;
    }

    delete boardThreads[thread];

    localStorage.watchedData = JSON.stringify(storedWatchedData);

  }

  cellWrapper.appendChild(cell);
  watcher.watchedMenu.appendChild(cellWrapper);

};

watcher.processOP = function(op) {

  var checkBox = op.getElementsByClassName('deletionCheckBox')[0];

  var nameParts = checkBox.name.split('-');

  var board = nameParts[0];
  var thread = nameParts[1];

  var watchButton = document.createElement('span');
  watchButton.className = 'watchButton glowOnHover icon';
  watchButton.title = "Watch Thread";

  checkBox.parentNode.insertBefore(watchButton,
      checkBox.nextSibling.nextSibling.nextSibling);

  watchButton.onclick = function(){ 
    watcher.addThread(board,thread,op);
  }

};

watcher.addThread = function(board,thread,op){
  var storedWatchedData = watcher.getStoredWatchedData();

  var boardThreads = storedWatchedData[board] || {};

  if (boardThreads[thread]) {
    return;
  }
  var subject;
  var message;
  var label;
  if(Array.isArray(op)){
    subject = op[0];
    message = op[1]; 
    label = (subject) ? subject : message;
  } else {
    subject = op.getElementsByClassName('labelSubject');
    message = op.getElementsByClassName('divMessage')[0];
    label = (subject.length ? subject[0].innerHTML : null)
      || message.innerHTML.replace(/(<([^>]+)>)/ig, "");
    label = label.substr(0, 26).trim();
  }
  
  if (!label.length) {
    label = null;
  } else {
    label = label.replace(/[<>"']/g, function(match) {
      return api.htmlReplaceTable[match]
    });
  }

  if(watcher.isInThread && thread === api.threadId){
    boardThreads[thread] = {
      lastSeenPostCount : document.getElementsByClassName("postCell").length,
      currentPostCount : document.getElementsByClassName("postCell").length,
      label : label
    };
  } else {
    boardThreads[thread] = {
      lastSeenPostCount : -1,
      currentPostCount : -1,
      label : label
    };
  }

  storedWatchedData[board] = boardThreads;

  localStorage.watchedData = JSON.stringify(storedWatchedData);

  watcher.addWatchedCell(board, thread, boardThreads[thread]);
}

watcher.init();
