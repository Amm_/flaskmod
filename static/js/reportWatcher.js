var reportWatcher = {};

reportWatcher.init = function() {
  	if(localStorage.reportCounter){
  		watcher.watcherAlertCounter = ocalStorage.reportCounter;
  	}
  	if(localStorage.viewReports){
  		reportWatcher.addNavButton();
  	}

  	reportWatcher.scheduleCheck();
}

reportWatcher.addNavButton = function(){
	var postingLink = document.getElementById('navPosting');
  	var referenceNode = postingLink.nextSibling;

  	var watcherButton = document.createElement('a');
  	watcherButton.id = "reportWatcherButton";
  	watcherButton.className = 'icon';
  	watcherButton.classList.add("reports");
  	watcherButton.title = "Report Watcher"
  	watcherButton.href = "/openReports.js";

  	reportWatcher.watcherCounter = document.createElement('span');

  	watcherButton.appendChild(reportWatcher.watcherCounter);

  	postingLink.parentNode.insertBefore(watcherButton, referenceNode);
  	reportWatcher.navButton = watcherButton;

  	if(localStorage.reportWatcherCounter) reportWatcher.watcherAlertCounter = localStorage.reportWatcherCounter;
	reportWatcher.updateWatcherCounter();
}

reportWatcher.updateWatcherCounter = function() {

  var classList = reportWatcher.navButton.classList;

  if (reportWatcher.watcherAlertCounter) {
    classList.add('mobileAlert');
  } else {
    classList.remove('mobileAlert');
  }

  reportWatcher.navButton.innerHTML = parseInt(reportWatcher.watcherAlertCounter) ? '('
      + reportWatcher.watcherAlertCounter + ')' : '';
};

reportWatcher.runCheck = function() {

  reportWatcher.watcherAlertCounter = 0;
  localStorage.lastReportCheck = new Date().getTime();

  api.formApiRequest('openReports', {}, function requestComplete(status,
      data) {

    if (status === 'ok') {
      reportWatcher.watcherAlertCounter = data.reports.length;
      localStorage.reportWatcherCounter = data.reports.length;
      if(reportWatcher.watcherAlertCounter){
      	if(!localStorage.viewReports) reportWatcher.addNavButton();
      	localStorage.viewReports = true;
      	reportWatcher.updateWatcherCounter();
      	reportWatcher.scheduleCheck();
      }
    } else {
    	delete localStorage.viewReports;
    }
  },true);
};

reportWatcher.scheduleCheck = function() {

  var lastCheck = localStorage.lastReportCheck;

  if (!lastCheck) {
    reportWatcher.runCheck();
    return;
  }

  lastCheck = new Date(+lastCheck);

  lastCheck.setUTCSeconds(lastCheck.getUTCSeconds() + 10);

  setTimeout(function() {
    reportWatcher.runCheck();
  }, lastCheck.getTime() - new Date().getTime());

}

reportWatcher.init();