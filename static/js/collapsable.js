var collapsable = {};

collapsable.init = function() {
	var extendablesList = document.getElementsByClassName("collapsable");
  Array.from(extendablesList).forEach(collapsable.addCollapsable);
};

collapsable.addCollapsable = function(extendableParent){
  extendableParent.classList.toggle('hidden');
  let toggled = false;

  let extra = document.getElementById(extendableParent.id + "Collapse");
  extra.classList.toggle('hidden');

  extendableParent.children[0].onclick = function() {

    extra.classList.toggle('hidden');
    extendableParent.children[0].classList.toggle('hidden');
    extendableParent.children[1].classList.toggle('hidden');

    toggled = !toggled;
  };
  extendableParent.children[1].onclick = extendableParent.children[0].onclick;
};

collapsable.init();
